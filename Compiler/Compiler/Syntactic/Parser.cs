﻿using Compiler.Exceptions.Parser;
using Compiler.Lexical;
using Compiler.Syntactic.Expressions;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Compiler.Syntactic
{
    public class Parser
    {
        public Queue<AbstractExpression> Parse(Queue<Token> tokens)
        {
            Queue<AbstractExpression> resultExpressionsQueue = new Queue<AbstractExpression>();
            List<Token> currentExpressionTokens = new List<Token>();
            foreach(Token token in tokens)
            {
                if (token.TokenType == TokenType.Semicolon)
                {
                    resultExpressionsQueue.Enqueue(AnalyseCurrentExpressionTokens(currentExpressionTokens));
                    currentExpressionTokens.Clear();
                }
                else
                {
                    currentExpressionTokens.Add(token);
                }
            }
            if(currentExpressionTokens.Count == 0)
                return resultExpressionsQueue;

            resultExpressionsQueue.Enqueue(AnalyseCurrentExpressionTokens(currentExpressionTokens));
            return resultExpressionsQueue;
        }

        private AbstractExpression AnalyseCurrentExpressionTokens(List<Token> currentExpressionTokens)
        {
            AbstractExpression expression = AnalyseAssignmentExpression(currentExpressionTokens);
            if (expression != null)
                return expression;

            expression = AnalyseDeclareExpression(currentExpressionTokens);
            if (expression != null)
                return expression;

            if(currentExpressionTokens.Any() && currentExpressionTokens[0].Value.Equals("print"))
            {
                expression = AnalyseOperationExpression(currentExpressionTokens);
                if (expression != null)
                    return expression;
            }

            throw new ParserException($"Invalid syntax: {string.Join(" ", currentExpressionTokens.Select(t => t.Value))}");
        }

        private AbstractExpression AnalyseLeftAssignmentValue(List<Token> currentExpressionTokens)
        {
            AbstractExpression rightValueExpression = AnalyseDeclareExpression(currentExpressionTokens);
            if (rightValueExpression != null)
                return rightValueExpression;

            rightValueExpression = AnalyseIdentExpression(currentExpressionTokens);
            if (rightValueExpression != null)
                return rightValueExpression;

            rightValueExpression = AnalysePropertyExpression(currentExpressionTokens);
            if (rightValueExpression != null)
                return rightValueExpression;

            throw new ParserException($"Invalid syntax: {string.Join(" ", currentExpressionTokens.Select(t => t.Value))}");
        }

        private AbstractExpression AnalyseRightAssignmentValue(List<Token> currentExpressionTokens)
        {
            AbstractExpression leftValueExpression = AnalyseSingleValueExpression(currentExpressionTokens);
            if (leftValueExpression != null)
                return leftValueExpression;

            leftValueExpression = AnalyseDefinitionExpression(currentExpressionTokens);
            if (leftValueExpression != null)
                return leftValueExpression;

            leftValueExpression = AnalyseOperationExpression(currentExpressionTokens);
            if (leftValueExpression != null)
                return leftValueExpression;

            throw new ParserException($"Invalid syntax: {string.Join(" ", currentExpressionTokens.Select(t => t.Value))}");
        }

        #region Assignment

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private AssignmentExpression AnalyseAssignmentExpression(List<Token> currentExpressionTokens)
        {
            int assignmentTokenIndex = currentExpressionTokens.FindIndex(t => t.TokenType == TokenType.Assignment);
            if (assignmentTokenIndex != -1)
            {
                List<Token> leftExpressionTokens = currentExpressionTokens.Take(assignmentTokenIndex).ToList();
                List<Token> rightExpressionTokens = currentExpressionTokens.Skip(assignmentTokenIndex + 1).Take(currentExpressionTokens.Count - assignmentTokenIndex - 1).ToList();
                return new AssignmentExpression(
                    allInnerTokens: currentExpressionTokens,
                    assignmentToken: currentExpressionTokens[assignmentTokenIndex],
                    leftValue: AnalyseLeftAssignmentValue(leftExpressionTokens),
                    rightValue: AnalyseRightAssignmentValue(rightExpressionTokens));
            }

            return null;
        }

        #region RightValue

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private DeclareExpression AnalyseDeclareExpression(List<Token> currentExpressionTokens)
        {
            if (currentExpressionTokens.Count == 2 &&
                currentExpressionTokens.First().TokenType == TokenType.Type &&
                currentExpressionTokens.Last().TokenType == TokenType.Identifier)
            {
                return new DeclareExpression(currentExpressionTokens, 
                    currentExpressionTokens.First(), 
                    currentExpressionTokens.Last());
            }

            return null;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private IdentExpression AnalyseIdentExpression(List<Token> currentExpressionTokens)
        {
            if (currentExpressionTokens.Count == 1 &&
                currentExpressionTokens.First().TokenType == TokenType.Identifier)
            {
                return new IdentExpression(currentExpressionTokens, currentExpressionTokens.First());
            }

            return null;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private PropertyExpression AnalysePropertyExpression(List<Token> currentExpressionTokens)
        {
            if (currentExpressionTokens.Count == 3 &&
                currentExpressionTokens[0].TokenType == TokenType.Identifier &&
                currentExpressionTokens[1].TokenType == TokenType.Dot &&
                currentExpressionTokens[2].TokenType == TokenType.Type)
            {
                return new PropertyExpression(currentExpressionTokens,
                    currentExpressionTokens.First(),
                    currentExpressionTokens.Last());
            }

            return null;
        }

        #endregion

        #region LeftValue

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private SingleValueExpression AnalyseSingleValueExpression(List<Token> currentExpressionTokens)
        {
            List<TokenType> possibleTokenTypes = new List<TokenType>
            {
                TokenType.Period, TokenType.Date, TokenType.PersonStatus, TokenType.IdentCode,
                TokenType.LicensePlate, TokenType.VIN, TokenType.Identifier, TokenType.Integer,
                TokenType.VehType, TokenType.Value, TokenType.PolicyStatus
            };

            if (currentExpressionTokens.Count == 1 &&
                possibleTokenTypes.Any(t => t == currentExpressionTokens.First().TokenType))
            {
                return new SingleValueExpression(currentExpressionTokens, currentExpressionTokens.First());
            }

            return null;
        }

        private DefinitionExpression AnalyseDefinitionExpression(List<Token> currentExpressionTokens)
        {
            bool isDefinition = currentExpressionTokens.Count > 3 &&
                currentExpressionTokens.First().TokenType == TokenType.Type &&
                currentExpressionTokens[1].TokenType == TokenType.OpenCurlyBrace &&
                currentExpressionTokens.Last().TokenType == TokenType.ClosedCurlyBrace;

            if (!isDefinition)
                return null;

            List<Token> currentExpression = new List<Token>();
            List<AbstractExpression> identExpressions = new List<AbstractExpression>();
            List<AbstractExpression> colonExpressions = new List<AbstractExpression>();
            for (int i = 2; i < currentExpressionTokens.Count - 1; i++)
            {
                if (currentExpressionTokens[i].TokenType == TokenType.Comma || 
                    i == currentExpressionTokens.Count - 2)
                {
                    if(i == currentExpressionTokens.Count - 2)
                        currentExpression.Add(currentExpressionTokens[i]);
                    identExpressions.Add(
                        new IdentExpression(currentExpression, currentExpression.First()));
                    colonExpressions.Add(
                        new ColonExpression(currentExpression, currentExpression.First(), currentExpression.Last()));
                    currentExpression = new List<Token>();
                    continue;
                }
                currentExpression.Add(currentExpressionTokens[i]);
            }

            if (identExpressions.Any() && identExpressions.All(e => AnalyseIdentExpression(e.AllInnerTokens) != null))
                return new DefinitionExpression(currentExpressionTokens, 
                    currentExpressionTokens.First(), identExpressions);

            if (colonExpressions.Any() && colonExpressions.All(e => AnalyseColonExpression(e.AllInnerTokens) != null))
                return new DefinitionExpression(currentExpressionTokens, 
                    currentExpressionTokens.First(), colonExpressions);

            return null;
        }

        private ColonExpression AnalyseColonExpression(List<Token> currentExpressionTokens)
        {
            bool isColonExpression = currentExpressionTokens.Count == 3 &&
                currentExpressionTokens[0].TokenType == TokenType.Type &&
                currentExpressionTokens[1].TokenType == TokenType.Colon;

            if (!isColonExpression)
                return null;

            List<TokenType> possibleTokenTypes = new List<TokenType>
            {
                TokenType.Period, TokenType.Date, TokenType.PersonStatus, TokenType.IdentCode,
                TokenType.LicensePlate, TokenType.VIN, TokenType.Identifier, TokenType.Integer,
                TokenType.VehType, TokenType.Value, TokenType.PolicyStatus
            };

            if(isColonExpression && possibleTokenTypes.Any(t => t == currentExpressionTokens[2].TokenType))
                return new ColonExpression(currentExpressionTokens,
                    currentExpressionTokens.First(),
                    currentExpressionTokens[1]);

            return null;
        }

        private OperationExpression AnalyseOperationExpression(List<Token> currentExpressionTokens)
        {
            bool satisfied = currentExpressionTokens.Count >= 4 &&
                currentExpressionTokens[0].TokenType == TokenType.Keyword &&
                currentExpressionTokens[1].TokenType == TokenType.OpenParenthesis &&
                currentExpressionTokens.Last().TokenType == TokenType.ClosedParenthesis;

            if (!satisfied)
                return null;

            List<Token> innerExpressionTokens =
                currentExpressionTokens.Skip(2).Take(currentExpressionTokens.Count - 3).ToList();

            AbstractExpression innerExpression = AnalyseIdentExpression(innerExpressionTokens);
            if (innerExpression == null)
                innerExpression = AnalyseDefinitionExpression(innerExpressionTokens);
            if (innerExpression == null)
                innerExpression = AnalyseOperationExpression(innerExpressionTokens);

            if (satisfied && innerExpression != null)
            {
                return new OperationExpression(currentExpressionTokens, 
                    currentExpressionTokens.First(), 
                    innerExpression);
            }

            return null;
        }

        #endregion

        #endregion
    }
}
