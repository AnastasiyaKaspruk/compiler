﻿using Compiler.Lexical;
using Compiler.Semantical;
using System;
using System.Collections.Generic;

namespace Compiler.Syntactic.Expressions
{
    public class DeclareExpression : AbstractExpression
    {
        public Token IdentTypeToken { get; private set; }
        public Token IdentToken { get; private set; }

        public DeclareExpression(List<Token> allInnerTokens,
            Token identTypeToken, 
            Token identToken) 
            : base(allInnerTokens)
        {
            IdentTypeToken = identTypeToken ?? throw new ArgumentNullException(nameof(identTypeToken));
            IdentToken = identToken ?? throw new ArgumentNullException(nameof(identToken));

            TreeOutput.Type = "declaration";
            TreeOutput.IdentType = IdentTypeToken.Value;
            TreeOutput.Name = IdentToken.Value;
        }

        public override bool IsValidSemantical(IReadOnlyDictionary<string, IdentedValue> identMemory)
        {
            return true;
        }
    }
}
