﻿using Compiler.Exceptions.Semantic;
using Compiler.Lexical;
using Compiler.Semantical;
using System;
using System.Collections.Generic;

namespace Compiler.Syntactic.Expressions
{
    public class IdentExpression : AbstractExpression
    {
        public Token IdentToken { get; private set; }

        public IdentExpression(List<Token> allInnerTokens,
            Token identToken)
            : base(allInnerTokens)
        {
            IdentToken = identToken ?? throw new ArgumentNullException(nameof(identToken));

            TreeOutput.Type = "identifier";
            TreeOutput.Name = IdentToken.Value;
        }

        public override bool IsValidSemantical(IReadOnlyDictionary<string, IdentedValue> identMemory)
        {
            if (!identMemory.TryGetValue(IdentToken.Value, out _))
                throw new SemanticException($"Using of unidentified variable '{IdentToken.Value}'");
            return true;
        }
    }
}
