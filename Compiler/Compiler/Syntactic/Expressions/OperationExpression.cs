﻿using Compiler.Exceptions.Semantic;
using Compiler.Lexical;
using Compiler.Semantical;
using System;
using System.Collections.Generic;

namespace Compiler.Syntactic.Expressions
{
    public class OperationExpression : AbstractExpression
    {
        public Token OperationToken { get; set; }
        public AbstractExpression InnerExpression { get; private set; }

        public OperationExpression(List<Token> allInnerTokens,
            Token operationToken,
            AbstractExpression innerExpression) 
            : base(allInnerTokens)
        {
            OperationToken = operationToken ?? throw new ArgumentNullException(nameof(operationToken));
            InnerExpression = innerExpression ?? throw new ArgumentNullException(nameof(innerExpression));

            TreeOutput.Type = "operation";
            TreeOutput.OperationType = OperationToken.Value;
            TreeOutput.Inner = InnerExpression.TreeOutput;
        }

        public override bool IsValidSemantical(IReadOnlyDictionary<string, IdentedValue> identMemory)
        {
            if(InnerExpression is IdentExpression identExpression)
            {
                if (!identMemory.TryGetValue(identExpression.IdentToken.Value, out IdentedValue identedValue))
                    throw new SemanticException($"Using of undefined variable: '{identExpression.IdentToken.Value}'");
                if(!identedValue.TypeToken.Value.Equals(TokenDefinitions.PolicyTypeValue))
                    throw new SemanticException($"The operation '{OperationToken.Value}' cannot be executed on type '{identedValue.TypeToken.Value}'");
                if (!identedValue.HasValue)
                    throw new SemanticException($"Using of undefined variable: '{identedValue.IdentToken.Value}'");

                return identExpression.IsValidSemantical(identMemory);
            }
            else if (InnerExpression is OperationExpression operationExpression)
            {
                return operationExpression.IsValidSemantical(identMemory);
            }

            return false;
        }
    }
}
