﻿using Compiler.Lexical;
using Compiler.Semantical;
using System;
using System.Collections.Generic;

namespace Compiler.Syntactic.Expressions
{
    public class SingleValueExpression : AbstractExpression
    {
        public Token ValueToken { get; private set; }

        public SingleValueExpression(List<Token> allInnerTokens,
            Token valueToken) 
            : base(allInnerTokens)
        {
            ValueToken = valueToken ?? throw new ArgumentNullException(nameof(valueToken));

            TreeOutput.Type = "value";
            TreeOutput.Value = ValueToken.Value;
        }

        public override bool IsValidSemantical(IReadOnlyDictionary<string, IdentedValue> identMemory)
        {
            return true;
        }
    }
}
