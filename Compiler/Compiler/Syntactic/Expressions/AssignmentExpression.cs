﻿using Compiler.Exceptions.Semantic;
using Compiler.Lexical;
using Compiler.Semantical;
using System;
using System.Collections.Generic;

namespace Compiler.Syntactic.Expressions
{
    public class AssignmentExpression : AbstractExpression
    {
        private static readonly Dictionary<string, Func<Token, bool>> semanticAnalysisDict =
            new Dictionary<string, Func<Token, bool>>
            {
                ["period"] = (t) => t.TokenType == TokenType.Period,
                ["start-date"] = (t) => t.TokenType == TokenType.Date,
                ["person-status"] = (t) => t.TokenType == TokenType.PersonStatus,
                ["ident-code"] = (t) => t.TokenType == TokenType.IdentCode ||
                    (t.TokenValueType == typeof(int) && t.Value.Length == 10),
                ["license-plate"] = (t) => t.TokenType == TokenType.LicensePlate,
                ["vin"] = (t) => t.TokenType == TokenType.VIN ||
                    (t.TokenType == TokenType.Identifier && t.Value.Length >= 5 && t.Value.Length <= 20),
                ["ins-premium"] = (t) => t.TokenType == TokenType.Integer,
                ["veh-type"] = (t) => t.TokenType == TokenType.VehType,
                ["mark"] = (t) => t.TokenType == TokenType.Identifier || t.TokenType == TokenType.Value,
                ["model"] = (t) => t.TokenType == TokenType.Identifier || t.TokenType == TokenType.Value,
                ["city"] = (t) => t.TokenType == TokenType.Identifier || t.TokenType == TokenType.Value,
                ["prod-year"] = (t) => t.TokenType == TokenType.Integer,
                ["policy-code"] = (t) => t.TokenType == TokenType.Integer,
                ["policy-status"] = (t) => t.TokenType == TokenType.PolicyStatus,
                ["date"] = (t) => t.TokenType == TokenType.Date,
                ["day"] = (t) => t.TokenType == TokenType.Integer,
                ["month"] = (t) => t.TokenType == TokenType.Integer,
                ["year"] = (t) => t.TokenType == TokenType.Integer
            };

        public Token AssignmentToken { get; private set; }

        public AbstractExpression RightValue { get; private set; }

        public AbstractExpression LeftValue { get; private set; }

        public AssignmentExpression(List<Token> allInnerTokens,
            Token assignmentToken,
            AbstractExpression leftValue,
            AbstractExpression rightValue)
            : base(allInnerTokens)
        {
            AssignmentToken = assignmentToken;
            RightValue = rightValue;
            LeftValue = leftValue;

            TreeOutput.Type = "assign";
            TreeOutput.Operator = AssignmentToken.Value;
            TreeOutput.Left = LeftValue.TreeOutput;
            TreeOutput.Right = RightValue.TreeOutput;
        }

        public override bool IsValidSemantical(IReadOnlyDictionary<string, IdentedValue> identMemory)
        {
            if(LeftValue is DeclareExpression declareExpression)
            {
                switch (RightValue)
                {
                    case DefinitionExpression definitionExpression:
                        if (!declareExpression.IdentTypeToken.Value.Equals(TokenDefinitions.PolicyTypeValue))
                            throw new SemanticException($"Invalid assignment to type: '{declareExpression.IdentTypeToken.Value}'");
                        return definitionExpression.IsValidSemantical(identMemory);
                    case OperationExpression operationExpression:
                        if (!declareExpression.IdentTypeToken.Value.Equals(TokenDefinitions.PolicyTypeValue))
                            throw new SemanticException($"Invalid assignment to type: '{declareExpression.IdentTypeToken.Value}'");
                        return operationExpression.IsValidSemantical(identMemory);
                    case SingleValueExpression singleValue:
                        if (!semanticAnalysisDict.TryGetValue(
                            declareExpression.IdentTypeToken.Value, 
                            out Func<Token, bool> tokenSemanticPredicate))
                            throw new SemanticException($"Invalid type of the identifier: '{declareExpression.IdentTypeToken.Value}'");
                        if(!tokenSemanticPredicate(singleValue.ValueToken))
                            throw new SemanticException($"Invalid assignment to type: '{declareExpression.IdentTypeToken.Value}'");
                        return singleValue.IsValidSemantical(identMemory);
                    default:
                        return false;
                }
            }
            else if(LeftValue is IdentExpression identExpression)
            {
                if(!identMemory.TryGetValue(identExpression.IdentToken.Value, out IdentedValue identedValue))
                    throw new SemanticException($"Using of undefined variable '{identExpression.IdentToken.Value}'");
                switch (RightValue)
                {
                    case DefinitionExpression definitionExpression:
                        if (!identedValue.TypeToken.Value.Equals(TokenDefinitions.PolicyTypeValue))
                            throw new SemanticException($"Invalid assignment for identifier '{identExpression.IdentToken.Value}' to type '{identedValue.TypeToken.Value}'");
                        return definitionExpression.IsValidSemantical(identMemory);
                    case OperationExpression operationExpression:
                        if (!identedValue.TypeToken.Value.Equals(TokenDefinitions.PolicyTypeValue))
                            throw new SemanticException($"Invalid assignment for identifier '{identExpression.IdentToken.Value}' to type '{identedValue.TypeToken.Value}'");
                        return operationExpression.IsValidSemantical(identMemory);
                    case SingleValueExpression singleValue:
                        if (!semanticAnalysisDict.TryGetValue(
                            identedValue.TypeToken.Value,
                            out Func<Token, bool> tokenSemanticPredicate))
                            throw new SemanticException($"Invalid type of the identifier: {identExpression.IdentToken.Value}");
                        if (!tokenSemanticPredicate(singleValue.ValueToken))
                            throw new SemanticException($"Invalid assignment to type: {identedValue.TypeToken.Value}");
                        return singleValue.IsValidSemantical(identMemory);
                    default:
                        return false;
                }
            }
            else if(LeftValue is PropertyExpression propertyExpression)
            {
                if(RightValue is SingleValueExpression singleValue)
                {
                    if (!semanticAnalysisDict.TryGetValue(
                            propertyExpression.TypeToken.Value,
                            out Func<Token, bool> tokenSemanticPredicate))
                        throw new SemanticException($"Invalid type: {propertyExpression.TypeToken.Value}");
                    if (!tokenSemanticPredicate(singleValue.ValueToken))
                        throw new SemanticException($"Invalid assignment to type: {propertyExpression.TypeToken.Value}");
                    return singleValue.IsValidSemantical(identMemory);
                }
                return false;
            }
            return false;
        }
    }
}
