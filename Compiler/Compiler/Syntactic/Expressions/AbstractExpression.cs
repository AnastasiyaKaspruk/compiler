﻿using Compiler.Lexical;
using Compiler.Semantical;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Compiler.Syntactic.Expressions
{
    public abstract class AbstractExpression
    {
        public dynamic TreeOutput { get; set; }

        public List<Token> AllInnerTokens { get; protected set; }

        public AbstractExpression(List<Token> allInnerTokens)
        {
            AllInnerTokens = allInnerTokens ?? throw new ArgumentNullException(nameof(allInnerTokens));
            TreeOutput = new JObject();
        }

        public abstract bool IsValidSemantical(IReadOnlyDictionary<string, IdentedValue> identMemory);

        public override string ToString()
        {
            return TreeOutput.ToString(Formatting.Indented);
        }
    }
}
