﻿using Compiler.Exceptions.Semantic;
using Compiler.Lexical;
using Compiler.Semantical;
using System;
using System.Collections.Generic;

namespace Compiler.Syntactic.Expressions
{
    public class ColonExpression : AbstractExpression
    {
        private static readonly Dictionary<string, Func<Token, bool>> semanticAnalysisDict =
            new Dictionary<string, Func<Token, bool>>
            {
                ["period"] = (t) => t.TokenType == TokenType.Period,
                ["start-date"] = (t) => t.TokenType == TokenType.Date,
                ["person-status"] = (t) => t.TokenType == TokenType.PersonStatus,
                ["ident-code"] = (t) => t.TokenType == TokenType.IdentCode ||
                    (t.TokenValueType == typeof(int) && t.Value.Length == 10),
                ["license-plate"] = (t) => t.TokenType == TokenType.LicensePlate,
                ["vin"] = (t) => t.TokenType == TokenType.VIN ||
                    (t.TokenType == TokenType.Identifier && t.Value.Length >= 5 && t.Value.Length <= 20),
                ["ins-premium"] = (t) => t.TokenType == TokenType.Integer,
                ["veh-type"] = (t) => t.TokenType == TokenType.VehType,
                ["mark"] = (t) => t.TokenType == TokenType.Identifier || t.TokenType == TokenType.Value,
                ["model"] = (t) => t.TokenType == TokenType.Identifier || t.TokenType == TokenType.Value,
                ["city"] = (t) => t.TokenType == TokenType.Identifier || t.TokenType == TokenType.Value,
                ["prod-year"] = (t) => t.TokenType == TokenType.Integer,
                ["policy-code"] = (t) => t.TokenType == TokenType.Integer,
                ["policy-status"] = (t) => t.TokenType == TokenType.PolicyStatus
            };

        public Token TypeToken { get; private set; }
        public Token ValueToken { get; private set; }

        public ColonExpression(List<Token> allInnerTokens,
            Token typeToken,
            Token valueToken) 
            : base(allInnerTokens)
        {
            TypeToken = typeToken ?? throw new ArgumentNullException(nameof(typeToken));
            ValueToken = valueToken ?? throw new ArgumentNullException(nameof(valueToken));

            TreeOutput.Type = "semicolon-def";
            TreeOutput.DefType = TypeToken.Value;
            TreeOutput.Value = ValueToken.Value;
        }

        public override bool IsValidSemantical(IReadOnlyDictionary<string, IdentedValue> identMemory)
        {
            if (!semanticAnalysisDict.TryGetValue(TypeToken.Value, out Func<Token, bool> semanticPredicate))
                throw new SemanticException($"Invalid type: '{TypeToken.Value}'");
            if(!semanticPredicate(ValueToken))
                throw new SemanticException($"Invalid value '{ValueToken.Value}' for type '{TypeToken.Value}'");
            return true;
        }
    }
}
