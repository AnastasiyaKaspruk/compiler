﻿using Compiler.Exceptions.Semantic;
using Compiler.Lexical;
using Compiler.Semantical;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Compiler.Syntactic.Expressions
{
    public class PropertyExpression : AbstractExpression
    {
        private static readonly List<string> policyPossibleTypes = new List<string>
        {
            "period", "start-date", "person-status", "ident-code",
            "license-plate", "vin", "ins-premium", "veh-type",
            "mark", "model", "city", "prod-year", "policy-code", "policy-status"
        };

        public Token IdentToken { get; private set; }
        public Token TypeToken { get; private set; }

        public PropertyExpression(List<Token> allInnerTokens,
            Token identToken,
            Token typeToken)
            : base(allInnerTokens)
        {
            IdentToken = identToken ?? throw new ArgumentNullException(nameof(identToken));
            TypeToken = typeToken ?? throw new ArgumentNullException(nameof(typeToken));

            TreeOutput.Type = "property-ref";
            TreeOutput.IdentName = IdentToken.Value;
            TreeOutput.RefType = TypeToken.Value;
        }

        public override bool IsValidSemantical(IReadOnlyDictionary<string, IdentedValue> identMemory)
        {
            if (!policyPossibleTypes.Any(t => t.Equals(TypeToken.Value)))
                throw new SemanticException($"Invalid property: '{TokenType.Value}'");
            return true;
        }
    }
}
