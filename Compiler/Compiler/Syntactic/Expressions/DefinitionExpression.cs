﻿using Compiler.Exceptions.Semantic;
using Compiler.Lexical;
using Compiler.Semantical;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Compiler.Syntactic.Expressions
{
    public class DefinitionExpression : AbstractExpression
    {
        private static readonly List<string> policyPossibleTypes = new List<string>
        {
            "period", "start-date", "person-status", "ident-code",
            "license-plate", "vin", "ins-premium", "veh-type",
            "mark", "model", "city", "prod-year", "policy-code", "policy-status"
        };
        public Token DefinedType { get; private set; }
        public List<AbstractExpression> InnerExpressions { get; private set; }

        public DefinitionExpression(List<Token> allInnerTokens,
            Token definedType,
            List<AbstractExpression> innerExpressions) 
            : base(allInnerTokens)
        {
            DefinedType = definedType ?? throw new ArgumentNullException(nameof(definedType));
            InnerExpressions = innerExpressions ?? throw new ArgumentNullException(nameof(innerExpressions));

            TreeOutput.Type = "definition";
            TreeOutput.DefinedType = DefinedType.Value;
            JArray innerArray = new JArray();
            foreach(AbstractExpression expression in InnerExpressions)
            {
                innerArray.Add(JToken.Parse(expression.ToString()));
            }
            TreeOutput.Inner = innerArray;
        }

        public override bool IsValidSemantical(IReadOnlyDictionary<string, IdentedValue> identMemory)
        {
            if (!DefinedType.Value.Equals(TokenDefinitions.PolicyTypeValue))
                throw new SemanticException($"Invalid definition type: '{DefinedType.Value}'");
            if(InnerExpressions.First() is ColonExpression)
                return InnerExpressions.All(e => e.IsValidSemantical(identMemory));   

            IdentExpression identExpression;
            foreach (AbstractExpression expression in InnerExpressions)
            {
                identExpression = expression as IdentExpression;
                if (!identMemory.TryGetValue(identExpression.IdentToken.Value, out IdentedValue identedValue))
                    throw new SemanticException($"Using of undefined variable: '{identExpression.IdentToken.Value}'");
                if(!identedValue.HasValue)
                    throw new SemanticException($"Using of uninitialized variable: '{identExpression.IdentToken.Value}'");
                if (!policyPossibleTypes.Any(t => t.Equals(identedValue.TypeToken.Value)))
                    throw new SemanticException($"Using variable of invalid type for the policy: '{identedValue.IdentToken.Value}'");
            }
            return true;
        }
    }
}
