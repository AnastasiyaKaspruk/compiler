﻿using System;

namespace Compiler.Lexical
{
    public class Token
    {
        public TokenType TokenType { get; private set; }

        public string Value { get; private set; }

        public Type TokenValueType { get; private set; }

        public Token(TokenType tokenType, string value)
        {
            TokenType = tokenType;
            Value = value ?? throw new ArgumentNullException(nameof(value));
            TokenValueType = typeof(string);
        }

        public Token(TokenType tokenType, string value, Type tokenValueType)
            : this(tokenType, value)
        {
            TokenValueType = tokenValueType ?? throw new ArgumentNullException(nameof(tokenValueType));
        }
    }
}
