﻿using Compiler.Exceptions.Lexer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Compiler.Lexical
{
    public class Lexer
    {
        public static Dictionary<string, TokenType> Delimiters =
            new Dictionary<string, TokenType>
            {
                [TokenDefinitions.Semicolon] = TokenType.Semicolon,
                [TokenDefinitions.Colon] = TokenType.Colon,
                [TokenDefinitions.Comma] = TokenType.Comma,
                [TokenDefinitions.Assignment] = TokenType.Assignment,
                [TokenDefinitions.Dot] = TokenType.Dot,
                [TokenDefinitions.OpenCurlyBrace] = TokenType.OpenCurlyBrace,
                [TokenDefinitions.ClosedCurlyBrace] = TokenType.ClosedCurlyBrace,
                [TokenDefinitions.OpenParenthesis] = TokenType.OpenParenthesis,
                [TokenDefinitions.ClosedParenthesis] = TokenType.ClosedParenthesis
            };

        public Queue<Token> GetTokens(string input)
        {
            List<string> inputChars = input.Select(c => c.ToString()).ToList();
            Queue<Token> tokens = new Queue<Token>();

            StringBuilder currentTokenValue = new StringBuilder();
            TokenType currentTokenType;
            Token currentToken;
            bool isInsideQuotedToken = false;
            foreach (string singeChar in inputChars)
            {
                if (isInsideQuotedToken)
                {
                    if (singeChar.Equals(TokenDefinitions.Quote))
                    {
                        AddToken();
                        isInsideQuotedToken = false;
                    }
                    else 
                    {
                        currentTokenValue.Append(singeChar);
                        continue;
                    } 
                }
                else if (singeChar.Equals(TokenDefinitions.Quote))
                {
                    isInsideQuotedToken = true;
                    continue;
                }
                else if (string.IsNullOrWhiteSpace(singeChar))
                {
                    AddToken();
                }
                else if (Delimiters.TryGetValue(singeChar, out currentTokenType))
                {
                    AddToken();
                    tokens.Enqueue(new Token(currentTokenType, singeChar));
                }
                else
                {
                    currentTokenValue.Append(singeChar);
                }
            }

            if (isInsideQuotedToken)
                throw new LexerException("Unclosed quoted('\"') token");

            return tokens;

            void AddToken()
            {
                currentToken = AnalyseCurrentToken(currentTokenValue.ToString());
                if (currentToken != null)
                    tokens.Enqueue(currentToken);
                currentTokenValue.Clear();
            }
        }

        private Token AnalyseCurrentToken(string currentToken)
        {
            if (string.IsNullOrWhiteSpace(currentToken))
                return null;

            if (TokenDefinitions.Types.Any(t => t.Equals(currentToken)))
                return new Token(TokenType.Type, currentToken);

            if (TokenDefinitions.Keywords.Any(k => k.Equals(currentToken)))
                return new Token(TokenType.Keyword, currentToken);

            if (TokenDefinitions.PersonStatuses.Any(s => s.Equals(currentToken)))
                return new Token(TokenType.PersonStatus, currentToken);

            if (TokenDefinitions.VehTypes.Any(t => t.Equals(currentToken)))
                return new Token(TokenType.VehType, currentToken);

            if (TokenDefinitions.PolicyStatuses.Any(s => s.Equals(currentToken)))
                return new Token(TokenType.PolicyStatus, currentToken);

            if (TokenDefinitions.Periods.Any(p => p.Equals(currentToken)) || IsPeriod(currentToken))
                return new Token(TokenType.Period, currentToken);

            if (DateTime.TryParseExact(currentToken, TokenDefinitions.DateFormat, null, DateTimeStyles.None, out DateTime date))
                return new Token(TokenType.Date, currentToken, typeof(DateTime));

            if (currentToken.Length == 10 && currentToken.All(c => char.IsDigit(c)))
                return new Token(TokenType.IdentCode, currentToken);       

            if (int.TryParse(currentToken, out int intValue))
                return new Token(TokenType.Integer, currentToken, typeof(int));

            if (currentToken.All(c => char.IsLetterOrDigit(c)))
            {
                string licensePlateTemplete = GetLicensePlateTemplete(currentToken);
                if (TokenDefinitions.LicensePlateTemplates.Any(t => t.Equals(licensePlateTemplete)))
                    return new Token(TokenType.LicensePlate, currentToken);

                if (Regex.IsMatch(currentToken, TokenDefinitions.VinRegExpPattern))
                    return new Token(TokenType.VIN, currentToken);

                return new Token(TokenType.Identifier, currentToken);
            }

            if (currentToken.All(c => char.IsLetterOrDigit(c) || string.IsNullOrWhiteSpace(c.ToString())))
                return new Token(TokenType.Value, currentToken);

            throw new LexerException($"Invalid token: {currentToken}");

            bool IsPeriod(string periodValue)
            {
                string[] periodParts = periodValue.Split(new char[] { '-' });
                
                return periodParts.Length == 2 &&
                    periodParts[1].Equals(TokenDefinitions.ShortMonthAlias) &&
                    int.TryParse(periodParts[0], out int monthsCount) &&
                    monthsCount > 0 && monthsCount < 12;
            }

            string GetLicensePlateTemplete(string inputValue)
            {
                StringBuilder template = new StringBuilder(inputValue.Length);
                foreach(char symbol in inputValue)
                {
                    if (char.IsDigit(symbol))
                        template.Append(TokenDefinitions.DigitTemplate);
                    else
                        template.Append(TokenDefinitions.LetterTemplate);
                }
                return template.ToString();
            }
        }
    }
}
