﻿using System.Collections.Generic;

namespace Compiler.Lexical
{
    public static class TokenDefinitions
    {
        public static string Semicolon = ";";

        public static string Colon = ":";

        public static string Comma = ",";

        public static string Assignment = "=";

        public static string Dot = ".";

        public static string Quote = "\"";

        public static string OpenCurlyBrace = "{";

        public static string ClosedCurlyBrace = "}";

        public static string OpenParenthesis = "(";

        public static string ClosedParenthesis = ")";

        public static List<string> Types = new List<string>
        {
            "policy", "period", "start-date", "person-status", "ident-code", 
            "license-plate", "vin", "ins-premium", "veh-type", "mark", 
            "model", "city", "prod-year", "policy-code", "policy-status", 
            "date", "day", "month", "year"
        };

        public static string PolicyTypeValue = "policy";

        public static List<string> Keywords = new List<string>
        {
            "new", "cancel-active", "reserve", "confirm", "cancel-reserve", 
            "duplicate", "history", "print", "read", "write"
        };

        public static List<string> Periods = new List<string>
        {
            "15-days", "1-year"
        };

        public static List<string> PersonStatuses = new List<string>
        {
            "individual", "legal-entity"
        };

        public static List<string> LicensePlateTemplates = new List<string>
        {
            "DDDDLL", "LLLDDD", "LLDDDDDD", "LLDDDDD", "LLDDDD", "LDDDDDD", "LDDDDD", "LLLDDDD",
            "DDDLDDDDD", "DDDLLDDDD", "DDDLLLDDDD", "LDLLDDDD", "LDDDDDLL", "DDDDDD", "DDLLDDDD",
            "DDDDDDLL", "LLDDDDLL", "LDDDDLL", "DDDDLLL", "DDDDDLL", "DDDDLD", "LLLLDDDD",
            "LLDDDD", "LLDDDDD", "LLLDDDD", "DDD"
        };

        public static string LetterTemplate = "L";

        public static string DigitTemplate = "D";

        public static List<string> VehTypes = new List<string>
        {
            "B1", "B2", "B3", "B4", "A1", "A2", "C1", "C2", "D1", "D2", "F", "E", "B5"
        };

        public static List<string> PolicyStatuses = new List<string>
        {
            "active", "reserved", "canceled", "duplicate"
        };

        public static string ShortMonthAlias = "mon";

        public static string VinRegExpPattern =
            "^(?=.*\\d+)[\\d,A,B,C,D,E,F,G,H,J,K,L,M,N,P,R,S,T,U,V,W,X,Y,Z]{5,20}$";

        public static string DateFormat = "dd-MM-yyyy";
    }
}
