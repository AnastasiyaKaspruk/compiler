﻿using System.ComponentModel;

namespace Compiler.Lexical
{
    public enum TokenType
    {
        [Description("Крапка з комою (;)")]
        Semicolon,
        [Description("Двокрапка (:)")]
        Colon,
        [Description("Кома (,)")]
        Comma,
        [Description("Знак присвоєння (=)")]
        Assignment,
        [Description("Крапка (.)")]
        Dot,
        [Description("Лапки (\")")]
        Quote,
        [Description("Відкрита фігурна дужка ({)")]
        OpenCurlyBrace,
        [Description("Закрита фігурна дужка (})")]
        ClosedCurlyBrace,
        [Description("Відкрита кругла дужка (()")]
        OpenParenthesis,
        [Description("Закрита кругла дужка ())")]
        ClosedParenthesis,
        [Description("Тип даних")]
        Type,
        [Description("Ключове слово")]
        Keyword,
        [Description("Період")]
        Period,
        [Description("Дата")]
        Date,
        [Description("Тип транспортного засобу")]
        VehType,
        [Description("Тип особи (фізична/юридична)")]
        PersonStatus,
        [Description("Статус полісу")]
        PolicyStatus,
        [Description("Ідентифікаційний код")]
        IdentCode,
        [Description("Держномер")]
        LicensePlate,
        [Description("VIN-код")]
        VIN,
        [Description("Значення")]
        Value,
        [Description("Ідентифікатор")]
        Identifier,
        [Description("Ціле число")]
        Integer
    }
}
