﻿using System;

namespace Compiler.Exceptions.Translation
{
    public class TranslatorException : Exception
    {
        public TranslatorException(string message)
            : base($"Translation error: {message}")
        {
        }
    }
}
