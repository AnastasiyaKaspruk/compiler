﻿using System;

namespace Compiler.Exceptions.Lexer
{
    public class LexerException : Exception
    {
        public LexerException(string message)
            : base($"Lexer error: {message}")
        {
        }
    }
}
