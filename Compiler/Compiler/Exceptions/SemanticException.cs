﻿using System;

namespace Compiler.Exceptions.Semantic
{
    public class SemanticException : Exception
    {
        public SemanticException(string message)
            : base($"Semantic error: {message}")
        {
        }
    }
}
