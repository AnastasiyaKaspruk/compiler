﻿using System;

namespace Compiler.Translation
{
    public class PolicyException : Exception
    {
        public PolicyException(string message) : base("Policy error: " + message)
        {
        }
    }
}
