﻿using System;

namespace Compiler.Exceptions.Parser
{
    public class ParserException : Exception
    {
        public ParserException(string message)
            : base($"Parser error: {message}")
        {
        }
    }
}
