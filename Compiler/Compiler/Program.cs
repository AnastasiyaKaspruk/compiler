﻿using Compiler.Exceptions.Lexer;
using Compiler.Exceptions.Parser;
using Compiler.Exceptions.Semantic;
using Compiler.Exceptions.Translation;
using Compiler.Lexical;
using Compiler.Semantical;
using Compiler.Syntactic;
using Compiler.Syntactic.Expressions;
using Compiler.Translation;
using System;
using System.Collections.Generic;
using System.IO;

namespace Compiler
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine();

            Lexer lexer = new Lexer();
            Parser parser = new Parser();

            var repoDirectory = Directory.GetParent(Directory.GetCurrentDirectory())
                .Parent.Parent.Parent.FullName;
            string validSampleCode = File.ReadAllText($"{repoDirectory}/Code-samples/sample-correct2.txt");

            try
            {
                Queue<Token> tokens = lexer.GetTokens(validSampleCode);
                Queue<AbstractExpression> expressions = parser.Parse(tokens);
                var analyser = new Analyser(expressions);
                if (analyser.IsValidSemantical())
                {
                    Translator translator = new Translator(analyser.IdentMemory);
                    translator.Translate(expressions);
                }   
            }
            catch (LexerException ex)
            {
                Console.WriteLine(ex);
            }
            catch (ParserException ex)
            {
                Console.WriteLine(ex);
            }
            catch(SemanticException ex)
            {
                Console.WriteLine(ex);
            }
            catch(TranslatorException ex)
            {
                Console.WriteLine(ex);
            }
            catch(PolicyException ex)
            {
                Console.WriteLine(ex);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                
                if(ex.InnerException != null)
                    Console.WriteLine(ex.InnerException.Message);
            }
        }
    }
}
