﻿using Compiler.Exceptions.Translation;
using Compiler.Lexical;
using Compiler.Semantical;
using Compiler.Syntactic.Expressions;
using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Compiler.Translation
{
    public class Translator
    {
        private static readonly string _periodEnumCode = 
            File.ReadAllText(@"..\..\Translation\Enums\Period.cs");

        private static readonly string _personStatusEnumCode = 
            File.ReadAllText(@"..\..\Translation\Enums\PersonStatus.cs");

        private static readonly string _policyStatusEnumCode = 
            File.ReadAllText(@"..\..\Translation\Enums\PolicyStatus.cs");

        private static readonly string _vehicleTypeEnumCode = 
            File.ReadAllText(@"..\..\Translation\Enums\VehicleType.cs");

        private static readonly string _policyCode = 
            File.ReadAllText(@"..\..\Translation\Policy.cs");

        private static readonly string _policyExceptionCode =
            File.ReadAllText(@"..\..\Exceptions\PolicyException.cs");

        private readonly IReadOnlyDictionary<string, IdentedValue> _identMemory;

        public Translator(IReadOnlyDictionary<string, IdentedValue> identMemory)
        {
            _identMemory = identMemory ?? throw new ArgumentException(nameof(identMemory));
        }

        public void Translate(Queue<AbstractExpression> codeExpressions)
        {
            string mainCode = FormMainCode(codeExpressions);
            CSharpCodeProvider provider = new CSharpCodeProvider();
            CompilerParameters parameters = new CompilerParameters();
            parameters.ReferencedAssemblies.Add("System.Core.dll");
            parameters.ReferencedAssemblies.Add(@"..\..\bin\Debug\Newtonsoft.Json.dll");
            parameters.GenerateInMemory = true;
            parameters.GenerateExecutable = true;
            CompilerResults results = provider.CompileAssemblyFromSource(parameters,
                _periodEnumCode,
                _personStatusEnumCode,
                _policyStatusEnumCode,
                _vehicleTypeEnumCode,
                _policyCode,
                _policyExceptionCode,
                mainCode);

            if (results.Errors.HasErrors)
            {
                StringBuilder sb = new StringBuilder();

                foreach (CompilerError error in results.Errors)
                {
                    sb.AppendLine(string.Format("Error ({0}): {1}", error.ErrorNumber, error.ErrorText));
                }

                throw new TranslatorException(sb.ToString());
            }

            Assembly assembly = results.CompiledAssembly;
            Type program = assembly.GetType("Compiler.Translation.Program");
            MethodInfo main = program.GetMethod("Main");
            main.Invoke(null, null);
        }

        private string FormMainCode(Queue<AbstractExpression> codeExpressions)
        {
            StringBuilder sb = new StringBuilder();

            foreach(AbstractExpression expression in codeExpressions)
            {
                if(expression is DeclareExpression declareExpression)
                {
                    sb.Append(FormDeclareExpressionString(declareExpression));
                }
                else if(expression is AssignmentExpression assignmentExpression)
                {
                    sb.Append(FormAssignmentExpressionString(assignmentExpression));
                }
                else if(expression is OperationExpression operationExpression)
                {
                    sb.Append(FormOperationExpression(operationExpression));
                }
                else
                {
                    throw new TranslatorException($"Unknown expression type: '{expression.GetType().Name}')");
                }

                sb.AppendLine(";");
            }

            return $@"
                using System;

                namespace Compiler.Translation
                {{
                    public class Program
                    {{
                        public static void Main()
                        {{
                            {sb.ToString()}
                        }}
                    }}
                }}
            ";
        }

        private string FormDeclareExpressionString(DeclareExpression declareExpression)
        {
            Dictionary<string, Func<Token, string>> declareExpressionsFormer =
                new Dictionary<string, Func<Token, string>>
                {
                    ["policy"] = (t) => $"Policy {t.Value}",
                    ["period"] = (t) => $"Period {t.Value}",
                    ["start-date"] = (t) => $"DateTime {t.Value}",
                    ["person-status"] = (t) => $"PersonStatus {t.Value}",
                    ["ident-code"] = (t) => $"string {t.Value}",
                    ["license-plate"] = (t) => $"string {t.Value}",
                    ["vin"] = (t) => $"string {t.Value}",
                    ["ins-premium"] = (t) => $"decimal {t.Value}",
                    ["veh-type"] = (t) => $"VehicleType {t.Value}",
                    ["mark"] = (t) => $"string {t.Value}",
                    ["model"] = (t) => $"string {t.Value}",
                    ["city"] = (t) => $"string {t.Value}",
                    ["prod-year"] = (t) => $"int {t.Value}",
                    ["policy-code"] = (t) => $"int {t.Value}",
                    ["policy-status"] = (t) => $"PolicyStatus {t.Value}",
                    ["date"] = (t) => $"DateTime {t.Value}",
                    ["day"] = (t) => $"int {t.Value}",
                    ["month"] = (t) => $"int {t.Value}",
                    ["year"] = (t) => $"int {t.Value}"
                };

            if (!declareExpressionsFormer.TryGetValue(declareExpression.IdentTypeToken.Value, out Func<Token, string> former))
                throw new TranslatorException($"Invalid type: '{declareExpression.IdentTypeToken.Value}'");

            return $"{former(declareExpression.IdentToken)}";
        }

        private string FormAssignmentExpressionString(AssignmentExpression assignmentExpression)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(FormLeftValueAssignmentExpressionSubstring(assignmentExpression.LeftValue));
            sb.Append(" = ");
            sb.Append(FormRightValueAssignmentExpressionSubstring(assignmentExpression.RightValue));

            return sb.ToString();
        }

        private string FormLeftValueAssignmentExpressionSubstring(AbstractExpression leftValue)
        {
            if(leftValue is IdentExpression identExpression)
            {
                return identExpression.IdentToken.Value;
            }
            else if(leftValue is DeclareExpression declareExpression)
            {
                return FormDeclareExpressionString(declareExpression).TrimEnd(';');
            }
            else if(leftValue is PropertyExpression propertyExpression)
            {
                Dictionary<string, string> propertyMatch = new Dictionary<string, string>
                {
                    ["period"] = nameof(Policy.Period),
                    ["start-date"] = nameof(Policy.StartDate),
                    ["person-status"] = nameof(Policy.PersonStatus),
                    ["ident-code"] = nameof(Policy.IdentCode),
                    ["license-plate"] = nameof(Policy.LicensePlate),
                    ["vin"] = nameof(Policy.VIN),
                    ["ins-premium"] = nameof(Policy.InsPremium),
                    ["veh-type"] = nameof(Policy.VehicleType),
                    ["mark"] = nameof(Policy.Mark),
                    ["model"] = nameof(Policy.Model),
                    ["city"] = nameof(Policy.City),
                    ["prod-year"] = nameof(Policy.ProdYear),
                    ["policy-code"] = nameof(Policy.PolicyCode),
                    ["policy-status"] = nameof(Policy.PolicyStatus),
                };

                if (!propertyMatch.TryGetValue(propertyExpression.TypeToken.Value, out string property))
                    throw new TranslatorException($"Invalid property: {propertyExpression.TypeToken.Value}");

                return $"{propertyExpression.IdentToken.Value}.{property}";
            }
            else
            {
                throw new TranslatorException($"Unknown expression type: '{leftValue.GetType().Name}')");
            }
        }

        private string FormRightValueAssignmentExpressionSubstring(AbstractExpression rightValue)
        {
            if(rightValue is SingleValueExpression singleValueExpression)
            {
                switch (singleValueExpression.ValueToken.TokenType)
                {
                    case TokenType.Period:
                        return MatchPeriod(singleValueExpression.ValueToken);
                    case TokenType.PersonStatus:
                        return MatchPersonStatus(singleValueExpression.ValueToken);
                    case TokenType.PolicyStatus:
                        return MatchPolicyStatus(singleValueExpression.ValueToken);
                    case TokenType.VehType:
                        return MatchVehicleType(singleValueExpression.ValueToken);
                    default:
                        return $"\"{singleValueExpression.ValueToken.Value}\"";
                }
            }
            else if(rightValue is DefinitionExpression definitionExpression)
            {
                return FormDefinitionExpression(definitionExpression);
            }
            else if(rightValue is OperationExpression operationExpression)
            {
                return FormOperationExpression(operationExpression);
            }
            else
            {
                throw new TranslatorException($"Unknown expression type: '{rightValue.GetType().Name}')");
            }
        }

        private string FormOperationExpression(OperationExpression operationExpression)
        {
            Stack<string> operations = new Stack<string>();
            operations.Push(operationExpression.OperationToken.Value);

            AbstractExpression currExp = operationExpression.InnerExpression;
            while (currExp is OperationExpression curOperExpression)
            {
                operations.Push(curOperExpression.OperationToken.Value);
                currExp = curOperExpression.InnerExpression;
            }

            IdentExpression identExpression = currExp as IdentExpression;

            if (identExpression == null)
                throw new TranslatorException($"Unknown expression type: '{currExp.GetType().Name}')");

            Dictionary<string, string> operationMatch = new Dictionary<string, string>
            {
                ["new"] = $"{nameof(Policy.New)}()",
                ["cancel-active"] = $"{nameof(Policy.CancelActive)}()",
                ["reserve"] = $"{nameof(Policy.Reserve)}()",
                ["confirm"] = $"{nameof(Policy.Confirm)}()",
                ["cancel-reserve"] = $"{nameof(Policy.CancelReserve)}()",
                ["duplicate"] = $"{nameof(Policy.Duplicate)}()",
                ["print"] = $"{nameof(Policy.Print)}()"
            };

            StringBuilder sb = new StringBuilder();
            sb.Append(identExpression.IdentToken.Value);

            foreach (string operation in operations)
            {
                sb.Append(".");

                if (!operationMatch.TryGetValue(operation, out string appendValue))
                    throw new TranslatorException($"Invalid operation: '{operation}'");

                sb.Append(appendValue);
            }

            return sb.ToString();
        }

        private string FormDefinitionExpression(DefinitionExpression definitionExpression)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("new Policy");
            sb.AppendLine("{");

            AbstractExpression firstInnerExpression = definitionExpression.InnerExpressions.FirstOrDefault();

            if (firstInnerExpression != null)
            {
                if (firstInnerExpression is ColonExpression)
                {
                    sb.AppendLine(FormColonInnerExpressions(definitionExpression.InnerExpressions));
                }
                else if (firstInnerExpression is IdentExpression)
                {
                    sb.AppendLine(FormIdentInnerExpressions(definitionExpression.InnerExpressions));
                }
                else
                {
                    throw new TranslatorException($"Unknown expression type: '{firstInnerExpression.GetType().Name}')");
                }
            }

            sb.Append("}");
            return sb.ToString();
        }

        private string FormColonInnerExpressions(List<AbstractExpression> colonInnerExpressions)
        {
            Dictionary<string, Func<Token, string>> defMatch =
               new Dictionary<string, Func<Token, string>>
               {
                   ["period"] = (t) => $"{nameof(Policy.Period)} = {MatchPeriod(t)}",
                   ["start-date"] = (t) => $"{nameof(Policy.StartDate)} = DateTime.ParseExact(\"{t.Value}\", \"dd-MM-yyyy\", null)",
                   ["person-status"] = (t) => $"{nameof(Policy.PersonStatus)} = {MatchPersonStatus(t)}",
                   ["ident-code"] = (t) => $"{nameof(Policy.IdentCode)} = \"{t.Value}\"",
                   ["license-plate"] = (t) => $"{nameof(Policy.LicensePlate)} = \"{t.Value}\"",
                   ["vin"] = (t) => $"{nameof(Policy.VIN)} = \"{t.Value}\"",
                   ["ins-premium"] = (t) => $"{nameof(Policy.InsPremium)} = {t.Value}",
                   ["veh-type"] = (t) => $"{nameof(Policy.VehicleType)} = {MatchVehicleType(t)}",
                   ["mark"] = (t) => $"{nameof(Policy.Mark)} = \"{t.Value}\"",
                   ["model"] = (t) => $"{nameof(Policy.Model)} = \"{t.Value}\"",
                   ["city"] = (t) => $"{nameof(Policy.City)} = \"{t.Value}\"",
                   ["prod-year"] = (t) => $"{nameof(Policy.ProdYear)} = {t.Value}",
                   ["policy-code"] = (t) => $"{nameof(Policy.PolicyCode)} = {t.Value}",
                   ["policy-status"] = (t) => $"{nameof(Policy.PolicyStatus)} = {MatchPolicyStatus(t)}"
               };

            StringBuilder sb = new StringBuilder();

            ColonExpression colonExpression;
            foreach (AbstractExpression abstractColonExpression in colonInnerExpressions)
            {
                colonExpression = abstractColonExpression as ColonExpression;

                if(colonExpression == null)
                    throw new TranslatorException($"Unknown expression type: '{abstractColonExpression.GetType().Name}')");

                if (!defMatch.TryGetValue(colonExpression.TypeToken.Value, out Func<Token, string> matchFunc))
                    throw new TranslatorException($"Unknown type: '{colonExpression.TypeToken.Value}')");

                sb.Append(matchFunc(colonExpression.ValueToken));
                sb.AppendLine(",");
            }

            return sb.ToString();
        }

        private string FormIdentInnerExpressions(List<AbstractExpression> identInnerExpressions)
        {
            Dictionary<string, Func<Token, string>> defMatch =
               new Dictionary<string, Func<Token, string>>
               {
                   ["period"] = (t) => $"{nameof(Policy.Period)} = {t.Value}",
                   ["start-date"] = (t) => $"{nameof(Policy.StartDate)} = {t.Value}",
                   ["person-status"] = (t) => $"{nameof(Policy.PersonStatus)} = {t.Value}",
                   ["ident-code"] = (t) => $"{nameof(Policy.IdentCode)} = ",
                   ["license-plate"] = (t) => $"{nameof(Policy.LicensePlate)} = {t.Value}",
                   ["vin"] = (t) => $"{nameof(Policy.VIN)} = {t.Value}",
                   ["ins-premium"] = (t) => $"{nameof(Policy.InsPremium)} = {t.Value}",
                   ["veh-type"] = (t) => $"{nameof(Policy.VehicleType)} = {t.Value}",
                   ["mark"] = (t) => $"{nameof(Policy.Mark)} = {t.Value}",
                   ["model"] = (t) => $"{nameof(Policy.Model)} = {t.Value}",
                   ["city"] = (t) => $"{nameof(Policy.City)} = {t.Value}",
                   ["prod-year"] = (t) => $"{nameof(Policy.ProdYear)} = {t.Value}",
                   ["policy-code"] = (t) => $"{nameof(Policy.PolicyCode)} = {t.Value}",
                   ["policy-status"] = (t) => $"{nameof(Policy.PolicyStatus)} = {t.Value}"
               };

            StringBuilder sb = new StringBuilder();

            IdentExpression identExpression;
            foreach (AbstractExpression abstractColonExpression in identInnerExpressions)
            {
                identExpression = abstractColonExpression as IdentExpression;

                if (identExpression == null)
                    throw new TranslatorException($"Unknown expression type: '{abstractColonExpression.GetType().Name}')");

                if(!_identMemory.TryGetValue(identExpression.IdentToken.Value, out IdentedValue identedValue))
                    throw new TranslatorException($"Unknown identifier: '{identExpression.IdentToken.Value}')");

                if (!defMatch.TryGetValue(identedValue.TypeToken.Value, out Func<Token, string> matchFunc))
                    throw new TranslatorException($"Unknown type: '{identExpression.IdentToken.Value}')");

                sb.Append(matchFunc(identExpression.IdentToken));
                sb.AppendLine(",");
            }

            return sb.ToString();
        }

        private string MatchPeriod(Token periodToken)
        {
            Dictionary<string, string> periodMatch = new Dictionary<string, string>
            {
                ["15-days"] = "Period.Days15",
                ["1-mon"] = "Period.Mon1",
                ["2-mon"] = "Period.Mon2",
                ["3-mon"] = "Period.Mon3",
                ["4-mon"] = "Period.Mon4",
                ["5-mon"] = "Period.Mon5",
                ["6-mon"] = "Period.Mon6",
                ["7-mon"] = "Period.Mon7",
                ["8-mon"] = "Period.Mon8",
                ["9-mon"] = "Period.Mon9",
                ["10-mon"] = "Period.Mon10",
                ["11-mon"] = "Period.Mon11",
                ["1-year"] = "Period.Year"
            };

            if(!periodMatch.TryGetValue(periodToken.Value, out string matchedValue))
                throw new TranslatorException($"Unknown period value: '{periodToken.Value}')");

            return matchedValue;
        }

        private string MatchPersonStatus(Token personStatusToken)
        {
            Dictionary<string, string> personStatusMatch = new Dictionary<string, string>
            {
                ["individual"] = "PersonStatus.Individual",
                ["legal-entity"] = "PersonStatus.LegalEntity"
            };

            if (!personStatusMatch.TryGetValue(personStatusToken.Value, out string matchedValue))
                throw new TranslatorException($"Unknown period value: '{personStatusToken.Value}')");

            return matchedValue;
        }

        private string MatchPolicyStatus(Token policyStatusToken)
        {
            Dictionary<string, string> policyStatusMatch = new Dictionary<string, string>
            {
                ["active"] = "PolicyStatus.Active",
                ["reserved"] = "PolicyStatus.Reserved",
                ["canceled"] = "PolicyStatus.Canceled",
                ["duplicate"] = "PolicyStatus.Duplicate"
            };

            if (!policyStatusMatch.TryGetValue(policyStatusToken.Value, out string matchedValue))
                throw new TranslatorException($"Unknown period value: '{policyStatusToken.Value}')");

            return matchedValue;
        }

        private string MatchVehicleType(Token vehicleTypeToken)
        {
            Dictionary<string, string> vehTypeMatch = new Dictionary<string, string>
            {
                ["B1"] = "VehicleType.B1",
                ["B2"] = "VehicleType.B2",
                ["B3"] = "VehicleType.B3",
                ["B4"] = "VehicleType.B4",
                ["A1"] = "VehicleType.A1",
                ["A2"] = "VehicleType.A2",
                ["C1"] = "VehicleType.C1",
                ["C2"] = "VehicleType.C2",
                ["D1"] = "VehicleType.D1",
                ["D2"] = "VehicleType.D2",
                ["F"] = "VehicleType.F",
                ["E"] = "VehicleType.E",
                ["B5"] = "VehicleType.B5"
            };

            if (!vehTypeMatch.TryGetValue(vehicleTypeToken.Value, out string matchedValue))
                throw new TranslatorException($"Unknown period value: '{vehicleTypeToken.Value}')");

            return matchedValue;
        }
    }
}
