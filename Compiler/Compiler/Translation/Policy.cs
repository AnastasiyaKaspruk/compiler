﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Compiler.Translation
{
    public class Policy
    {
        public Period? Period { get; set; }
        public DateTime? StartDate { get; set; }
        public PersonStatus? PersonStatus { get; set; }
        public string IdentCode { get; set; }
        public string LicensePlate { get; set; }
        public string VIN { get; set; }
        public decimal? InsPremium { get; set; }
        public VehicleType? VehicleType { get; set; }
        public string Mark { get; set; }
        public string Model { get; set; }
        public string City { get; set; }
        public int? ProdYear { get; set; }
        public int? PolicyCode { get; set; }
        public PolicyStatus? PolicyStatus { get; set; }

        public Policy New()
        {
            if (!Period.HasValue)
            {
                throw new PolicyException("'period' property should be filled to perform 'new' operation");
            }
            if (!StartDate.HasValue)
            {
                throw new PolicyException("'start-date' property should be filled to perform 'new' operation");
            }
            if (!PersonStatus.HasValue)
            {
                throw new PolicyException("'person-status' property should be filled to perform 'new' operation");
            }
            if (string.IsNullOrWhiteSpace(IdentCode))
            {
                throw new PolicyException("'ident-code' property should be filled to perform 'new' operation");
            }
            if (string.IsNullOrWhiteSpace(LicensePlate))
            {
                throw new PolicyException("'license-plate' property should be filled to perform 'new' operation");
            }
            if (string.IsNullOrWhiteSpace(VIN))
            {
                throw new PolicyException("'vin' property should be filled to perform 'new' operation");
            }
            if (!InsPremium.HasValue)
            {
                throw new PolicyException("'ins-premium' property should be filled to perform 'new' operation");
            }
            if (!VehicleType.HasValue)
            {
                throw new PolicyException("'veh-type' property should be filled to perform 'new' operation");
            }
            if (string.IsNullOrWhiteSpace(Mark))
            {
                throw new PolicyException("'mark' property should be filled to perform 'new' operation");
            }
            if (string.IsNullOrWhiteSpace(Model))
            {
                throw new PolicyException("'model' property should be filled to perform 'new' operation");
            }
            if (string.IsNullOrWhiteSpace(City))
            {
                throw new PolicyException("'city' property should be filled to perform 'new' operation");
            }
            if (!ProdYear.HasValue)
            {
                throw new PolicyException("'prod-year' property should be filled to perform 'new' operation");
            }
            if (PolicyStatus.HasValue)
            {
                throw new PolicyException("Cannot perform 'new' operation on policy with status '{PolicyStatus.ToString()}'");
            }

            Random random = new Random(Environment.TickCount);
            PolicyCode = random.Next(1, 1000000);
            PolicyStatus = Translation.PolicyStatus.Active;

            return this;
        }

        public Policy CancelActive()
        {
            if (!PolicyStatus.HasValue || !PolicyStatus.Equals(Translation.PolicyStatus.Active))
                throw new PolicyException("Cannot perform 'cancel-active' operation on policy with status '" + (PolicyStatus.HasValue ? PolicyStatus.ToString() : "null") + "'");

            PolicyStatus = Translation.PolicyStatus.Canceled;

            return this;
        }

        public Policy Reserve()
        {
            if(PolicyStatus.HasValue)
                throw new PolicyException("Cannot perform 'reserve' operation on policy with status '" + PolicyStatus.ToString() + "'");

            PolicyStatus = Translation.PolicyStatus.Reserved;

            return this;
        }

        public Policy Confirm()
        {
            if (!PolicyStatus.HasValue || !PolicyStatus.Equals(Translation.PolicyStatus.Reserved))
                throw new PolicyException("Cannot perform 'confirm' operation on policy with status '" + (PolicyStatus.HasValue ? PolicyStatus.ToString() : "null") + "'");

            PolicyStatus = Translation.PolicyStatus.Active;

            return this;
        }

        public Policy CancelReserve()
        {
            if (!PolicyStatus.HasValue || !PolicyStatus.Equals(Translation.PolicyStatus.Reserved))
                throw new PolicyException("Cannot perform 'cancel-reserve' operation on policy with status '" + (PolicyStatus.HasValue ? PolicyStatus.ToString() : "null") + "'");

            PolicyStatus = Translation.PolicyStatus.Canceled;

            return this;
        }

        public Policy Duplicate()
        {
            if (!PolicyStatus.HasValue || !PolicyStatus.Equals(Translation.PolicyStatus.Active))
                throw new PolicyException("Cannot perform 'duplicate' operation on policy with status '" + (PolicyStatus.HasValue ? PolicyStatus.ToString() : "null") + "'");

            Policy duplicate = this.MemberwiseClone() as Policy;
            duplicate.PolicyStatus = Translation.PolicyStatus.Duplicate;

            return duplicate;
        }

        public void Print()
        {
            string policy = JsonConvert.SerializeObject(this);
            JToken policyTokens = JToken.Parse(policy);

            Console.WriteLine(policyTokens.ToString(Formatting.Indented));
        }
    }
}
