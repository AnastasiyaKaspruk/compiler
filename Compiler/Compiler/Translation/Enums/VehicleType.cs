﻿namespace Compiler.Translation
{
    public enum VehicleType
    {
        B1 = 1,
        B2 = 2,
        B3 = 3,
        B4 = 4,
        A1 = 5,
        A2 = 6,
        C1 = 7,
        C2 = 8,
        D1 = 9,
        D2 = 10,
        F = 11,
        E = 12,
        B5 = 13
    }
}
