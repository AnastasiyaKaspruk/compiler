﻿namespace Compiler.Translation
{
    public enum PersonStatus
    {
        Individual = 1,
        LegalEntity = 2
    }
}
