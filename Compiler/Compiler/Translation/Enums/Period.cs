﻿namespace Compiler.Translation
{
    public enum Period
    {
        Days15 = 0,
        Mon1 = 1,
        Mon2 = 2,
        Mon3 = 3,
        Mon4 = 4,
        Mon5 = 5,
        Mon6 = 6,
        Mon7 = 7,
        Mon8 = 8,
        Mon9 = 9,
        Mon10 = 10,
        Mon11 = 11,
        Mon12 = 12,
        Year = 13
    }
}
