﻿namespace Compiler.Translation
{
    public enum PolicyStatus
    {
        Active = 1,
        Reserved = 2,
        Canceled = 3,
        Duplicate = 4
    }
}
