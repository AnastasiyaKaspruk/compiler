﻿using Compiler.Lexical;

namespace Compiler.Semantical
{
    public class IdentedValue
    {
        public Token TypeToken { get; set; }
        public Token IdentToken { get; set; }
        public bool HasValue { get; set; }
    }
}
