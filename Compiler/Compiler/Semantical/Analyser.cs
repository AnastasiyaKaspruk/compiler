﻿using Compiler.Exceptions.Semantic;
using Compiler.Syntactic.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Compiler.Semantical
{
    public class Analyser
    {
        private readonly Queue<AbstractExpression> _innerExpressions;
        public IReadOnlyDictionary<string, IdentedValue> IdentMemory { get; }

        public Analyser(Queue<AbstractExpression> innerExpressions)
        {
            _innerExpressions = innerExpressions ?? throw new ArgumentNullException(nameof(innerExpressions));
            IdentMemory = FillMemory();
        }

        public bool IsValidSemantical()
        {
            return _innerExpressions.All(e => e.IsValidSemantical(IdentMemory));
        }

        public Dictionary<string, IdentedValue> FillMemory()
        {
            Dictionary<string, IdentedValue> identValues = new Dictionary<string, IdentedValue>();
            foreach(AbstractExpression expression in _innerExpressions)
            {
                if(expression is DeclareExpression declareExpression)
                {
                    if (identValues.TryGetValue(declareExpression.IdentToken.Value, out _))
                        throw new SemanticException(
                            $"Identifier '{declareExpression.IdentToken.Value}' already initialized in the current context");
                    identValues.Add(
                        declareExpression.IdentToken.Value,
                        new IdentedValue
                        {
                            IdentToken = declareExpression.IdentToken,
                            TypeToken = declareExpression.IdentTypeToken,
                            HasValue = false
                        });
                    continue;
                }
                if(expression is AssignmentExpression assignmentExpression)
                {
                    switch (assignmentExpression.LeftValue)
                    {
                        case DeclareExpression leftDeclare:
                            if (identValues.TryGetValue(leftDeclare.IdentToken.Value, out _))
                                throw new SemanticException(
                                    $"Identifier '{leftDeclare.IdentToken.Value}' already initialized in the current context");
                            identValues.Add(
                                leftDeclare.IdentToken.Value,
                                new IdentedValue
                                {
                                    IdentToken = leftDeclare.IdentToken,
                                    TypeToken = leftDeclare.IdentTypeToken,
                                    HasValue = true
                                });
                            break;
                        case IdentExpression leftIdent:
                            identValues.TryGetValue(leftIdent.IdentToken.Value, out IdentedValue identedLeftValue);
                            if(identedLeftValue == null)
                                throw new SemanticException($"Using of undefined variable '{leftIdent.IdentToken.Value}'");
                            identedLeftValue.HasValue = true;
                            break;
                    }
                        
                }
            }
            return identValues;
        }
    }
}
